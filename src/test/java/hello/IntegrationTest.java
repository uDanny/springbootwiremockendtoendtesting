package hello;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(
    properties = "api=http://localhost:8081/stub/get/")
public class IntegrationTest {

    private static final String path ="/stub/get/London&APPID=904ea80590393efff354c8503811aeff";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8081);

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testMvcOK() {
        ServletContext servletContext = wac.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("weatherController"));
    }



    @Test
    public void testEndToEndWithMocked3rdPartyOK() throws Exception {
        //mocking the 3rd party API
        stubFor(get(urlPathMatching("/stub/get/London&APPID=904ea80590393efff354c8503811aeff"))
                    .willReturn(aResponse()
                                    .withStatus(200)
                                    .withHeader("Content-Type", "application/json")
                                    .withBody("{\"test-status\": \"WireMock Success\"}")));


        //calling our Rest endpoint
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/get/London"))
                                          .andDo(print()).andExpect(status().isOk())
                                          .andExpect(jsonPath("$.test-status").value("WireMock Success"))
                                          .andReturn();


        //ResultAction builder alternative, JUnit approach for testing
        assertEquals("{\"test-status\": \"WireMock Success\"}",mvcResult.getResponse().getContentAsString() );
        assertEquals(200,mvcResult.getResponse().getStatus() );

        //check url was called
        verify(getRequestedFor(urlEqualTo(path)));
    }
}
