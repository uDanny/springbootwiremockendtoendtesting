package hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import hello.service.ApiResolver;

@RestController
public class WeatherController {

    @Autowired
    private ApiResolver apiResolver;

    @GetMapping(value = "/get/{city}")
    public String getByCity(@PathVariable("city") String city) {
        return apiResolver.getWeather(city);
    }
}
