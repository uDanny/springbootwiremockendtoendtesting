package hello;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import hello.service.ApiResolver;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(
    properties = "api=http://localhost:8081/stub/get/")
    public class RestStubTest {

    private static final String path ="/stub/get/London&APPID=904ea80590393efff354c8503811aeff";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8081);

    @Autowired
    private ApiResolver apiResolver;

    @Test
    public void testStubByCallingServiceLayerOK() {

        stubFor(get(urlPathMatching("/stub/get/London&APPID=904ea80590393efff354c8503811aeff"))
                    .willReturn(aResponse()
                                    .withStatus(200)
                                    .withHeader("Content-Type", "application/json")
                                    .withBody("\"test-status\": \"WireMock Success\"")));

        String london = apiResolver.getWeather("London");

        assertEquals("\"test-status\": \"WireMock Success\"",london);

        verify(getRequestedFor(urlEqualTo(path)));
    }

    @Test
    public void testStubByCallingServiceLayerKO() {

        stubFor(get(urlPathMatching("/stub/get/London&APPID=904ea80590393efff354c8503811aeff"))
                    .willReturn(aResponse()
                                    .withStatus(400)));

        String london = apiResolver.getWeather("London");

        assertEquals("Unexpected exception thrown",london);
    }
}
