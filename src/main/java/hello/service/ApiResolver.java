package hello.service;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ApiResolver {
    private static final String API_KEY = "&APPID=904ea80590393efff354c8503811aeff";

    @Value("${api}")
    private String url;

    public String getWeather(String city) {
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url + city + API_KEY);
            CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() != 200){
                throw new RuntimeException("very big error!!");
            }
            HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity);
        }catch (Exception e){
            return "Unexpected exception thrown";
        }
    }



}
